Name: mummer
Version: 3.23
Release: 2
Summary: 'MUMmer is a program for rapidly aligning large genomes'
License: PerlArtistic
Requires: perl
Source0: http://downloads.sourceforge.net/project/mummer/mummer/3.23/MUMmer3.23.tar.gz
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%define pkgname mummer

%prep
 

%description
'MUMmer is a program for rapidly aligning large genomes'

%build
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd MUMmer${pkgver};
    make

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd MUMmer${pkgver};
    mkdir -p ${pkgdir}/usr/share/doc/mummer ${pkgdir}/usr/bin ${pkgdir}/usr/lib/mummer/{aux_bin,scripts} ${pkgdir}/usr/share/man/man1;
    install -m755 annotate combineMUMs delta-filter dnadiff exact-tandems gaps mapview mgaps mummer mummerplot nucmer nucmer2xfig promer repeat-match run-mummer1 run-mummer3 show-aligns show-coords show-diff show-snps show-tiling ${pkgdir}/usr/bin;
    install -m755 aux_bin/{prenuc,postnuc,prepro,postpro} ${pkgdir}/usr/lib/mummer/aux_bin;
    install -m755 scripts/*.{pl,awk,csh} ${pkgdir}/usr/lib/mummer/scripts;
    cp -r README ACKNOWLEDGEMENTS docs/*{.README,.pdf} docs/web ${pkgdir}/usr/share/doc/mummer;
    install -Dm644 scripts/Foundation.pm ${pkgdir}/usr/lib/perl5/site_perl/Foundation.pm;
    find ${pkgdir} -type f -exec sed -i -e "s@$PWD/scripts@/usr/lib/mummer/scripts@g" {} \;;
    find ${pkgdir} -type f -exec sed -i -e "s@$PWD/aux_bin@/usr/lib/mummer/aux_bin@g" {} \;;
    find ${pkgdir} -type f -exec sed -i -e "s@$PWD@/usr/bin@g" {} \;;
    mv ${pkgdir}/usr/bin/{,mummer-}annotate;
    sed -i -e 's@\$bindir/annotate@\$bindir/mummer-annotate@' ${pkgdir}/usr/bin/run-mummer1 ${pkgdir}/usr/lib/mummer/scripts/run-mummer1.csh

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

